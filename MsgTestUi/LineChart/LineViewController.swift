//
//  LineViewController.swift
//  MsgTestUi
//
//  Created by Nan on 01/05/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import UIKit
import QuartzCore

class LineViewController: UIViewController {

    var label = UILabel()
    var lineChart: LineChart!
    
    @IBOutlet weak var chartUiView: UIView!
    @IBOutlet weak var chartWidth: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chartView()
    }
    
    func chartView() {
        var views: [String: AnyObject] = [:]
        
        label.text = "..."
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.center
        self.view.addSubview(label)
        views["label"] = label
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[label]-|", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-80-[label]", options: [], metrics: nil, views: views))
        
        // simple arrays
        let data: [CGFloat] = [3, 4, 2, 11, 13, 15, 35,3, 4, 2, 11, 13, 15, 35, 3, 4, 2, 11, 13, 15, 35,3, 4, 2, 11, 13, 15, 35]
        //        let data2: [CGFloat] = [1, 3, 5, 13, 17, 20]
        
        // simple line with custom x axis labels
        let xLabels: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July","Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "July","Jan", "Feb", "Mar", "Apr", "May", "Jun", "July"]
        
        lineChart = LineChart()
        lineChart.animation.enabled = true
        lineChart.area = false
        lineChart.x.labels.visible = true
        lineChart.x.grid.count = 10
        lineChart.y.grid.count = 10
        lineChart.x.labels.values = xLabels
        //        lineChart.y.labels.values = xLabels
        lineChart.y.labels.visible = true
        lineChart.addLine(data)
        //        lineChart.addLine(data2)
        lineChart.translatesAutoresizingMaskIntoConstraints = false
        lineChart.delegate = self
        chartWidth.constant = CGFloat(data.count * 30)
        chartUiView.layoutIfNeeded()
        lineChart.frame = CGRect(x: 10, y: 0, width: (chartUiView.frame.width - 10), height: (chartUiView.frame.height - 50))
        chartUiView.addSubview(lineChart)
//        self.view.addSubview(lineChart)
        views["chart"] = lineChart
//        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[chart]-|", options: [], metrics: nil, views: views))
//        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[label]-[chart(==200)]", options: [], metrics: nil, views: views))
        
        //        var delta: Int64 = 4 * Int64(NSEC_PER_SEC)
        //        var time = dispatch_time(DISPATCH_TIME_NOW, delta)
        //
        //        dispatch_after(time, dispatch_get_main_queue(), {
        //            self.lineChart.clear()
        //            self.lineChart.addLine(data2)
        //        });
        
        //        var scale = LinearScale(domain: [0, 100], range: [0.0, 100.0])
        //        var linear = scale.scale()
        //        var invert = scale.invert()
        //        println(linear(x: 2.5)) // 50
        //        println(invert(x: 50)) // 2.5
    }
}

extension LineViewController: LineChartDelegate{
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    /**
     * Line chart delegate method.
     */
    func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>) {
        label.text = "x: \(x)     y: \(yValues)"
    }
    
    
    
    /**
     * Redraw chart on device rotation.
     */
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if let chart = lineChart {
            chart.setNeedsDisplay()
        }
    }
}
