//
//  ViewController.swift
//  MsgTestUi
//
//  Created by Nan on 29/04/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AssetsLibrary

class ViewController: UIViewController {

    @IBOutlet weak var msgField: UITextField!
    @IBOutlet weak var scrollview: UIView!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    @IBOutlet weak var scroll: UIScrollView!
    var msgData = [[String:AnyObject]]()
    var msg = [String]()
    var msgHeight = [CGFloat]()
    var tapImage = UIImage()
    var txtEtcHeight = 130
    var imgEtcHeight = 230
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
        msgHeight.append(20)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func actionSend(_ sender: Any) {
        if(msgField.text != ""){
            msg.append(msgField.text!)
            showOutgoingMessage(text: msgField.text!, flag: true)
            msgField.text = ""
        }
        else if(msgField.text == " "){
            print("Space")
        }
        else{
            print("Empty")
        }
        msgField.resignFirstResponder()
    }
    
    @IBAction func actionClear(_ sender: Any) {
        self.scrollview.subviews.forEach { $0.removeFromSuperview() }
        self.scrollview.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        msgHeight.removeAll()
        msg.removeAll()
        msgHeight.append(20)
    }
    
    @IBAction func actionLoadRemoteMsg(_ sender: Any) {
        getMsg()
    }
    
    @IBAction func actionAttachFile(_ sender: Any) {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self
        myPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPicController, animated: true, completion: nil)
    }
    
    @IBAction func actionAlert(_ sender: Any) {
        let alertVc = UIAlertController()
        let alertAc = UIAlertAction(title: "Action1", style: .default, handler: nil)
        let alertAc1 = UIAlertAction(title: "Action2", style: .default, handler: nil)
        alertAc1.setValue(UIColor.gray, forKey: "titleTextColor")
        let alertAc2 = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
        alertVc.addAction(alertAc)
        alertVc.addAction(alertAc1)
        alertVc.addAction(alertAc2)
        self.present(alertVc, animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func showOutgoingMessage(text: String, flag: Bool) {
//        let guide = view.safeAreaLayoutGuide
//        let label =  UILabel()
        let label =  CopyLabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = text
        label.isUserInteractionEnabled = true
        
        let constraintRect = CGSize(width: 0.66 * view.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: label.font!],
                                            context: nil)
        label.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))
        
        let bubbleSize = CGSize(width: label.frame.width + 28,
                                height: label.frame.height + 20)
        
        let width = bubbleSize.width
        let height = bubbleSize.height
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: width - 22, y: height))
        bezierPath.addLine(to: CGPoint(x: 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: 0, y: 17))
        bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
        bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
        bezierPath.close()
        
        let outgoingMessageLayer = CAShapeLayer()
        outgoingMessageLayer.path = bezierPath.cgPath
        outgoingMessageLayer.frame = CGRect(x: scrollview.frame.width - (width + 10),
                                            y: msgHeight.reduce(0, +),
                                            width: width,
                                            height: height)
//        outgoingMessageLayer.fillColor = UIColor(red: 0.09, green: 0.54, blue: 1, alpha: 1).cgColor
            outgoingMessageLayer.fillColor = #colorLiteral(red: 0.462745098, green: 0.7215686275, blue: 0.5882352941, alpha: 1)
        scrollview.layer.addSublayer(outgoingMessageLayer)
        label.frame = CGRect(x: (outgoingMessageLayer.frame.minX + 10), y: outgoingMessageLayer.frame.minY, width: (outgoingMessageLayer.frame.width - 15), height: (outgoingMessageLayer.frame.height - 2))
        scrollview.addSubview(label)
        scrollHeight.constant = msgHeight.reduce(0, +) + CGFloat(txtEtcHeight)
        scrollview.layoutIfNeeded()
        msgHeight.append(height + 20)
        if(msgHeight.reduce(0, +) > scroll.frame.height){
            let bottomOffset = CGPoint(x: 0, y: scroll.contentSize.height - scroll.bounds.size.height)
            scroll.setContentOffset(bottomOffset, animated: true)
        }
        
        if(flag){
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.reply(txtMsg: label.text?.lowercased() as! String, imgMsg: nil)
            })
        }
    }
    
    func showIncomingMessage(text: String) {
//        let label =  UILabel()
        let label =  CopyLabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = text
        
        let constraintRect = CGSize(width: 0.66 * view.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: label.font!],
                                            context: nil)
        label.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))
        
        let bubbleSize = CGSize(width: label.frame.width + 28,
                                height: label.frame.height + 20)
        
        let width = bubbleSize.width
        let height = bubbleSize.height
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 22, y: height))
        bezierPath.addLine(to: CGPoint(x: width - 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: width, y: height - 17), controlPoint1: CGPoint(x: width - 7.61, y: height), controlPoint2: CGPoint(x: width, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: width, y: 17))
        bezierPath.addCurve(to: CGPoint(x: width - 17, y: 0), controlPoint1: CGPoint(x: width, y: 7.61), controlPoint2: CGPoint(x: width - 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: 4, y: 17), controlPoint1: CGPoint(x: 11.61, y: 0), controlPoint2: CGPoint(x: 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height), controlPoint1: CGPoint(x: 4, y: height - 1), controlPoint2: CGPoint(x: 0, y: height))
        bezierPath.addLine(to: CGPoint(x: -0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: 11.04, y: height - 4.04), controlPoint1: CGPoint(x: 4.07, y: height + 0.43), controlPoint2: CGPoint(x: 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: 22, y: height), controlPoint1: CGPoint(x: 16, y: height), controlPoint2: CGPoint(x: 19, y: height))
        bezierPath.close()
        
        let incomingMessageLayer = CAShapeLayer()
        incomingMessageLayer.path = bezierPath.cgPath
        incomingMessageLayer.frame = CGRect(x: 10,
                                            y: msgHeight.reduce(0, +),
                                            width: width,
                                            height: height)
//        incomingMessageLayer.fillColor = UIColor(red: 0.09, green: 0.54, blue: 1, alpha: 1).cgColor
        incomingMessageLayer.fillColor = #colorLiteral(red: 0.6980392157, green: 0.6039215686, blue: 0.7568627451, alpha: 1)
        scrollview.layer.addSublayer(incomingMessageLayer)
        label.frame = CGRect(x: (incomingMessageLayer.frame.minX + 10), y: incomingMessageLayer.frame.minY, width: (incomingMessageLayer.frame.width - 15), height: (incomingMessageLayer.frame.height - 2))
        scrollview.addSubview(label)
        scrollHeight.constant = msgHeight.reduce(0, +) + CGFloat(txtEtcHeight)
        scrollview.layoutIfNeeded()
        msgHeight.append(height + 20)
        if(msgHeight.reduce(0, +) > scroll.frame.height){
            let bottomOffset = CGPoint(x: 0, y: scroll.contentSize.height - scroll.bounds.size.height)
            scroll.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    func showIncomingImageMsg(imageView: UIImage){
        
        let width: CGFloat = 200
        let height: CGFloat = 200

        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 22, y: height))
        bezierPath.addLine(to: CGPoint(x: width - 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: width, y: height - 17), controlPoint1: CGPoint(x: width - 7.61, y: height), controlPoint2: CGPoint(x: width, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: width, y: 17))
        bezierPath.addCurve(to: CGPoint(x: width - 17, y: 0), controlPoint1: CGPoint(x: width, y: 7.61), controlPoint2: CGPoint(x: width - 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: 4, y: 17), controlPoint1: CGPoint(x: 11.61, y: 0), controlPoint2: CGPoint(x: 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height), controlPoint1: CGPoint(x: 4, y: height - 1), controlPoint2: CGPoint(x: 0, y: height))
        bezierPath.addLine(to: CGPoint(x: -0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: 11.04, y: height - 4.04), controlPoint1: CGPoint(x: 4.07, y: height + 0.43), controlPoint2: CGPoint(x: 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: 22, y: height), controlPoint1: CGPoint(x: 16, y: height), controlPoint2: CGPoint(x: 19, y: height))
        bezierPath.close()
        
        let incomingMessageLayer = CAShapeLayer()
        incomingMessageLayer.path = bezierPath.cgPath
        let imageView = UIImageView(image: imageView)
        imageView.frame = CGRect(x: 10,
                                 y: msgHeight.reduce(0, +),
                                 width: width,
                                 height: height)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.mask = incomingMessageLayer
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(actionImage(sender:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)
        
        scrollview.addSubview(imageView)
        scrollHeight.constant = msgHeight.reduce(0, +) + CGFloat(imgEtcHeight)
        scrollview.layoutIfNeeded()
        msgHeight.append(height + 20)
        if(msgHeight.reduce(0, +) > scroll.frame.height){
//            let bottomOffset = CGPoint(x: 0, y: scroll.contentSize.height - scroll.bounds.size.height)
//            scroll.setContentOffset(bottomOffset, animated: true)
            toBottom()
        }
    }
    
    func showOutgoingImageMsg(imageView: UIImage, flag: Bool){
        let width: CGFloat = 200
        let height: CGFloat = 200
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: width - 22, y: height))
        bezierPath.addLine(to: CGPoint(x: 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: 0, y: 17))
        bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
        bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
        bezierPath.close()
        
        let incomingMessageLayer = CAShapeLayer()
        incomingMessageLayer.path = bezierPath.cgPath
        let imageView = UIImageView(image: imageView)
        imageView.frame = CGRect(x: scrollview.frame.width - (width + 10),
                                 y: msgHeight.reduce(0, +),
                                 width: width,
                                 height: height)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.mask = incomingMessageLayer
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(actionImage(sender:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)
        
        scrollview.addSubview(imageView)
        scrollHeight.constant = msgHeight.reduce(0, +) + CGFloat(imgEtcHeight)
        scrollview.layoutIfNeeded()
        msgHeight.append(height + 20)
        if(msgHeight.reduce(0, +) > scroll.frame.height){
//            let bottomOffset = CGPoint(x: 0, y: scroll.contentSize.height - scroll.bounds.size.height)
//            scroll.setContentOffset(bottomOffset, animated: true)
            toBottom()
        }
        
        if(flag){
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.reply(txtMsg: "" as! String, imgMsg: imageView.image)
            })
        }
    }
}

extension ViewController : UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if(msgField.text != ""){
//            msg.append(msgField.text!)
//            showOutgoingMessage(text: msgField.text!)
//            msgField.text = ""
//        }
//        else if(msgField.text == " "){
//            print("Space")
//        }
//        else{
//            print("Empty")
//        }
        msgField.resignFirstResponder()
        return true
    }
    
    func reply(txtMsg: String, imgMsg: UIImage?){
//        print(txtMsg)
        switch txtMsg {
        case "hi":
            showIncomingMessage(text: "Hello")
            break
        case "?":
            showIncomingMessage(text: "What does it mean ??")
            break
        case "how are you":
            showIncomingMessage(text: "I m fine, how about You ?")
            break
        case "fine":
            showIncomingMessage(text: "oh! Great")
            break
        case "i am fine":
            showIncomingMessage(text: "oh! Great")
            break
        case "hey":
            showIncomingMessage(text: "Yup, What?")
            break
        case "😘":
            showIncomingMessage(text: "😘😘😘")
            break
        case "😍":
            showIncomingMessage(text: "😍😍😍")
            break
        case "😏":
            showIncomingMessage(text: "😏😏😏")
            break
        case "":
            print("\"\"")
            break
        default:
            showIncomingMessage(text: "No idea")
        }
        
        if(imgMsg != nil){
            switch imgMsg {
            case imgMsg:
                showIncomingImageMsg(imageView: imgMsg!)
                break
            default:
                showIncomingMessage(text: "No idea")
            }
        }
    }
    
    func getMsg(){
        let para: Parameters = ["defect_ticket_id_primary": 1]
        Alamofire.request("http://192.168.0.104/Source/Defect/defect_reply.php" , method: .post, parameters: para).responseJSON { (responce) in
            if (responce.result.value) != nil{
                if let data = JSON(responce.result.value ?? "").arrayObject{
                    self.msgData = data as! [[String : AnyObject]]
//                    print(self.msgData)
                    if(self.msgData.count > 0){
                        self.getOldMsg(oldMsg: self.msgData)
                    }
                }
            }
        }
    }
    
    func getOldMsg(oldMsg: [[String:AnyObject]]){
        for i in 0...(oldMsg.count - 1){
            let msg = oldMsg[i]
            if(msg["created_by"]! as! String == "1"){
                showOutgoingMessage(text: msg["def_ticket_reply_note"] as! String, flag: false)
                let chk = Configure(value: msg["def_ticket_reply_file"] as? String)
                if(chk != "0"){
                    let rev = chk.replacingOccurrences(of: "[\\[\\]\"]", with: "", options: .regularExpression, range: nil)
                    let imgArray = rev.split(separator: ",")
                    for i in 0...(imgArray.count - 1){
                        let img = imgArray[i]
                        let url = URL(string: "http://192.168.0.104/Source/Image/\(img)")
                        let data = try? Data(contentsOf: url!)
                        if let imageData = data {
                            let image = UIImage(data: imageData)
                            showOutgoingImageMsg(imageView: image!, flag: false)
                        }

                    }
                }
            }
            else{
                showIncomingMessage(text: Configure(value: msg["def_ticket_reply_note"] as? String))
                let chk = Configure(value: msg["def_ticket_reply_file"] as? String)
                if(chk != "0"){
                    let rev = chk.replacingOccurrences(of: "[\\[\\]\"]", with: "", options: .regularExpression, range: nil)
                    let imgArray = rev.split(separator: ",")
                    for i in 0...(imgArray.count - 1){
                        let img = imgArray[i]
                        let url = URL(string: "http://192.168.0.104/Source/Image/\(img)")
                        let data = try? Data(contentsOf: url!)
                        if let imageData = data {
                            let image = UIImage(data: imageData)
                            showIncomingImageMsg(imageView: image!)
                        }
                        
                    }
                }
            }
        }
    }
    
    func Configure(value: String?) -> String{
        if value == nil || value == "null" || value is NSNull || value == ""{
            return "0"
        }else{
            return value!
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let imgToData = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        let imgToSet = imgToData.cgImage
//        self.imgFileData = imgToData.jpegData(compressionQuality: 0.2)
//        upImg.image = UIImage(cgImage: imgToSet!)
        showOutgoingImageMsg(imageView: UIImage(cgImage: imgToSet!), flag: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func actionImage(sender:UITapGestureRecognizer){
        let tappedImage = sender.view as! UIImageView
        tapImage = tappedImage.image!
        self.performSegue(withIdentifier: "ViewImageViewController", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! ViewImageViewController
        dest.img = tapImage
    }
    
    func toBottom() {
        var offset = scroll.contentOffset
        offset.y = (scrollview.frame.height - scroll.frame.height) + CGFloat(imgEtcHeight)
        scroll.setContentOffset(offset, animated: true)
    }
}
