//
//  ViewImageViewController.swift
//  MsgTestUi
//
//  Created by Nan on 03/05/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import UIKit

class ViewImageViewController: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    var img: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgView.image = img
    }

}
