//
//  AutocellViewController.swift
//  MsgTestUi
//
//  Created by Nan on 29/04/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import UIKit

class AutocellViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var tblView: UITableView!
    let cellid = "cellid"
    var celldata : [msgBubble] = [msgBubble]()
//    var celldata : [TxtMsg] = [TxtMsg]()

    override func viewDidLoad() {
        super.viewDidLoad()
//        staticMsgs()
        showIncomingMessage(text: "Hello ")
        tblView.register(UITableViewCell.self, forCellReuseIdentifier: cellid)
        // Do any additional setup after loading the view.
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return celldata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath)
        let currentMsg = celldata[indexPath.row]
        currentMsg.bubble.frame = cell.contentView.frame
//        currentMsg.bubble.actions = [0.0, 1.0]
//        cell.contentView.layer.insertSublayer(currentMsg.bubble, at: 0)
//        cell.textLabel?.text = currentMsg.msg
        cell.layer.insertSublayer(currentMsg.bubble, at: 0)
        
        return cell
    }
    
    func staticMsgs() {
//        celldata.append(TxtMsg(msg: "Helllo", usr: "me", date: "date"))
//        celldata.append(TxtMsg(msg: "Hiii", usr: "me", date: "date"))
//        celldata.append(TxtMsg(msg: "How r u", usr: "me", date: "date"))
//        celldata.append(TxtMsg(msg: "Nahhh", usr: "me", date: "date"))
        
        
    }
    
    func showIncomingMessage(text: String) {
        let label =  UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.text = text
        
        let constraintRect = CGSize(width: 0.66 * view.frame.width,
                                    height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: label.font],
                                            context: nil)
        label.frame.size = CGSize(width: ceil(boundingBox.width),
                                  height: ceil(boundingBox.height))
        
        let bubbleSize = CGSize(width: label.frame.width + 28,
                                height: label.frame.height + 20)
        
        let width = bubbleSize.width
        let height = bubbleSize.height
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 22, y: height))
        bezierPath.addLine(to: CGPoint(x: width - 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: width, y: height - 17), controlPoint1: CGPoint(x: width - 7.61, y: height), controlPoint2: CGPoint(x: width, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: width, y: 17))
        bezierPath.addCurve(to: CGPoint(x: width - 17, y: 0), controlPoint1: CGPoint(x: width, y: 7.61), controlPoint2: CGPoint(x: width - 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: 4, y: 17), controlPoint1: CGPoint(x: 11.61, y: 0), controlPoint2: CGPoint(x: 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height), controlPoint1: CGPoint(x: 4, y: height - 1), controlPoint2: CGPoint(x: 0, y: height))
        bezierPath.addLine(to: CGPoint(x: -0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: 11.04, y: height - 4.04), controlPoint1: CGPoint(x: 4.07, y: height + 0.43), controlPoint2: CGPoint(x: 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: 22, y: height), controlPoint1: CGPoint(x: 16, y: height), controlPoint2: CGPoint(x: 19, y: height))
        bezierPath.close()
        
        let outgoingMessageLayer = CAShapeLayer()
        outgoingMessageLayer.path = bezierPath.cgPath
        outgoingMessageLayer.frame = CGRect(x: 10,
                                            y: 350,
                                            width: width,
                                            height: height)
        outgoingMessageLayer.fillColor = UIColor(red: 0.09, green: 0.54, blue: 1, alpha: 1).cgColor
//        scrollview.layer.addSublayer(outgoingMessageLayer)
//        outgoingMessageLayer.addSublayer(label.layer)
        label.frame = CGRect(x: (outgoingMessageLayer.frame.minX + 10), y: outgoingMessageLayer.frame.minY, width: (outgoingMessageLayer.frame.width - 15), height: (outgoingMessageLayer.frame.height - 2))
//        //        label.center = scrollview.center
//        scrollview.addSubview(label)
        celldata.append(msgBubble(bubble: outgoingMessageLayer))
//        celldata.append(msgBubble(bubble: outgoingMessageLayer))
//        celldata.append(msgBubble(bubble: outgoingMessageLayer))
    }
    
}
