//
//  CellData.swift
//  MsgTestUi
//
//  Created by Nan on 30/04/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import Foundation
import UIKit

struct TxtMsg {
    var msg : String
    var usr : String
    var date : String
}

struct  ImgMsg {
    var img : UIImage
    var usr : String
    var date : String
}

struct msgBubble {
    var bubble : CALayer
}
